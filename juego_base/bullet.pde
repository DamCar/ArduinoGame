class Bullet extends Point
{
  private PImage img;
  private float w=50;
  private String texture="../../assets/bullet.png";
  public Bullet()
  {
    super();
    img=loadImage(texture);
  }
  public Bullet(float x, float y)
  {
    super(x,y);
    img=loadImage(texture);
  }
  public void Set(float x, float y)
  {
    X(x);
    Y(y);
    image(img,X(),Y(),w,w);
  }
  public void Set()
  {
    image(img,X(),Y(),w,w);
  }
  public void accelerate(float acc)
  {
    X(X()+acc);
    Set();
  }
  
  
  
  
}