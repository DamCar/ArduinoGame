class Nave extends Point
{
	private PImage img;
  private float w=50;
  private String texture="../../assets/nave.png";
  private boolean up=false;
	public Nave()
	{
		super();
		img=loadImage(texture);
	}
	public Nave(float x, float y)
	{
		super(x,y);
		img=loadImage(texture);
	}
  public void Set(float x, float y)
  {
    X(x);
    Y(y);
    image(img,X(),Y(),w,w);
  }
  public void Set()
  {
    image(img,X(),Y(),w,w);
  }
  public void scan()
  {
    //go down
    if(key=='s'&& !up)
    {
      n.Y(n.Y()+w);
      up=true;
    }
    //go down
    if(key=='w'&&up)
    {
      n.Y(n.Y()-w);
      up=false;
    }
  }
  
}