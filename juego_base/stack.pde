class bulletStack
{
  ArrayList<Bullet>bullets;
  int lim;
  // limite de distancia a recorrer
  float wlimit;
  //Constructor sin limite
  public bulletStack(float w)
  {
    wlimit=w;
    lim=0;
    bullets=new ArrayList<Bullet>();
  }
  //Constructor con limite
  public  bulletStack(int l,float w)
  {
    wlimit=w;
    lim =l;
    bullets=new ArrayList<Bullet>();
  }
  public boolean isEmpty(){return bullets.size()<1;}
  public boolean isFull(){return lim>0?bullets.size()==lim:false;}
  public boolean push(Bullet item)
  {
    if(!isFull())
    {
      bullets.add(item);
      return true;
    }
    return false;     
  }
  
  public Bullet Get(int index){return bullets.get(index);}
  public void accelerate()
  {
    if(!isEmpty())
      for(int i=0;i<bullets.size();i++)
      {
        if(Get(i).X()<wlimit)
        {
          Get(i).X(Get(i).X()+2);
          Get(i).Set();
        }
        else
        {
           bullets.remove(i);
           if(i!=0 &&i<bullets.size()-1)
             i--;
        }
      }
  }
}