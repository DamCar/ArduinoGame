class Point
{
	private float x,y;
	public Point(float x, float y)
	{
		this.x=x;
		this.y=y;
	}
	public Point()
	{
		this.x=0;
		this.y=0;
	}
	public float X(){return x;}
	public void X(float x){this.x=x;}
	public float Y(){return y;}
	public void Y(float y){this.y=y;}
}