#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#define POWER 3
#define BUNO  2
#define BDOS  5
#define TIME 200

// Clase Punto
class Punto
{
  public:
    int x,y;
    Punto(int x, int y)
    {
      this->x=x;
      this->y=y;
    }
};

//Clase Nave
class Nave 
{
  public:
  byte nave[8]={0b11111100,0b11111100,0b11111100,0b11111111,0b11111111,0b11111100,0b11111100,0b11111100};
  bool isUp;
  LiquidCrystal_I2C * lcd;
  Nave(LiquidCrystal_I2C * lcd)
  {
    this->lcd=lcd;
    this->lcd->createChar(0,this->nave);
    this->isUp=true;
  }
  void draw()
  {
    this->lcd->setCursor(0,this->isUp?1:0);
    this->lcd->write(0);
  }
  
};

//Clase Bala
class Bala : public Punto
{
  public:
  LiquidCrystal_I2C * lcd;
  Bala(LiquidCrystal_I2C * lcd,int x=0,int y=0):Punto(int(x),int(y))
  {
    this->lcd=lcd;
  }
  void draw()
  {
    this->lcd->setCursor(this->x,this->y);
    this->lcd->write(1);
  }
  ~Bala(){}
};
//Vector de balas
class BalaVector
{
  public:
  Bala *balas[3];
  int wlimit=20;
  int num=0;
  int lim=3;
  BalaVector(){}
  bool isFull(){return num==lim;}
  bool isEmpty(){return num==0;}
  void push(Bala item)
  {
    if(!isFull())
    {
      balas[num]=&item;
      num++;
    }
  }
  void pop()
  {
    if(!isEmpty())
    {
      for(int i =1;i<num;i++)
      {
        balas[i-1]=balas[i];
        balas[i]=NULL;
      }
      num--;
    }
  }
  void move()
  {
    if(!isEmpty())
    {
      for(int i=0;i<num;i++)
        if(balas[i]->x>=wlimit-1)
          pop();
        else
          balas[i]->x++;
      for(int i=0;i<num;i++)
        balas[i]->draw();
    }
  }
};




byte bala[8]={
  0b00000000,
  0b00000000,
  0b00000000,
  0b11111111,
  0b11111111,
  0b00000000,
  0b00000000,
  0b00000000
  };

LiquidCrystal_I2C *lcd;
Nave *n;
BalaVector *v;
bool btn1(){return digitalRead(BUNO);}
bool btn2(){return digitalRead(BDOS);}
void setup()
{
  lcd=new LiquidCrystal_I2C(0x27, 20, 4);
  lcd->begin();
  lcd->clear();
  lcd->setCursor(0,0);
  lcd->print("Iniciando...");
  lcd->createChar(1,bala);
  v=new BalaVector();
  n=new Nave(lcd);
  pinMode(BUNO,INPUT);
  pinMode(BDOS,INPUT);
  delay(500);
  lcd->print("Listo!");
  delay(500);
}

void loop()
{
  lcd->clear();
  n->isUp=btn2();
  n->draw();
  if(btn1())
    v->push(Bala(lcd,1,n->isUp));
  v->move();
  delay(TIME);
}


